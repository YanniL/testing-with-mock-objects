package Tester;

import static org.easymock.EasyMock.createMock; 
import static org.easymock.EasyMock.replay; 
import static org.easymock.EasyMock.expect; 
import static org.easymock.EasyMock.verify;
import static org.easymock.EasyMock.expectLastCall;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Web.ConnectionFactory;
import Web.WebClient_2;

public class TestWebClientEasyMock
{
    private ConnectionFactory factory;

    private InputStream stream;

    @Before
    public void setUp()
    {
        factory = createMock("factory", ConnectionFactory.class );
        stream = createMock("stream", InputStream.class );
    }

    @Test
    public void testGetContentOk()
        throws Exception
    {
        expect( factory.getData() ).andReturn( stream );
        expect( stream.read() ).andReturn( new Integer( (byte) 'W' ) );
        expect( stream.read() ).andReturn( new Integer( (byte) 'o' ) );
        expect( stream.read() ).andReturn( new Integer( (byte) 'r' ) );
        expect( stream.read() ).andReturn( new Integer( (byte) 'k' ) );
        expect( stream.read() ).andReturn( new Integer( (byte) 's' ) );
        expect( stream.read() ).andReturn( new Integer( (byte) '!' ) );

        expect( stream.read() ).andReturn( -1 );
        stream.close();

        replay( factory );
        replay( stream );

        WebClient_2 client = new WebClient_2();

        String result = client.getContent( factory );

        assertEquals( "Works!", result );
    }
    
    @After
    public void tearDown()
    {
        verify( factory );
        verify( stream );
    }
}
